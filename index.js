// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';

const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
const requestOne = require('request');

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));

  function welcome(agent) {
    agent.add(`Welkom bij de enige echte Appwise bot! Waar kan ik je mee helpen?`);
  }

  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }

  function testIntent(agent) {
    agent.add('Test received, sending test back');
  }

function newReservation(agent) {
    var persons = request.body.queryResult.parameters['numberOfPersons'];
    var time = request.body.queryResult.parameters['time'];
    var time = request.body.queryResult.parameters['time'];
    var numberOfPersons = 0;
    if (containsNumber(persons)) {
        numberOfPersons = persons.match(/\d+/)[0];
    } else {
        numberOfPersons = stringToNumber(persons);
    }
    //<speak>First sentence. <break time="1s"/> Second sentence. <prosody pitch="+5st">and now high pitched</prosody></speak>
    var responseId = agent.responseId;
    agent.add('Number of persons: ' + numberOfPersons);
}

  function clientArrived(agent) {
      let audioFileToReturn = '<audio src="https://s3-eu-west-1.amazonaws.com/kinderverhalen-template/speech_20180802162001146.mp3"/>';
      // let text_to_speech = '<speak>\n' +
      //     '    <prosody rate="slow">Aaaah super!</prosody> <break time="1.0s"/>\n' +
      //     ' <prosody volume="loud">Hey klant!!!  </prosody> <break time="1.0s"/>\n' +
      //     ' <prosody volume="x-loud">Welkom bij </prosody>\n' +
      //     ' <prosody rate="slow" volume="x-loud">Appwise </prosody>\n' +
      //     '<audio src="https://s3-eu-west-1.amazonaws.com/kinderverhalen-template/applause.ogg"/>\n' +
      //     '<break time="1.0s"/>\n' +
      //     '  Kan ik jou misschien iets te drinken aanbieden?\n' +
      //     '</speak>';
      agent.add(audioFileToReturn)
  }

  function offerDrink(agent) {
      let slackLink = "https://hooks.slack.com/services/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";
      var no = request.body.queryResult.parameters['no'];
      if (no === "no") {
          agent.add("oke, geen drank voor jou.... blijf maar dorstig");
      } else {
          //stuur bericht naar Olga
          var drink = request.body.queryResult.parameters['drink'];
          var number = request.body.queryResult.parameters['number'];
          let textForSlack = request.body.queryResult.queryText;
          //OR USE DRINKREQUEST PARAMETER

          requestOne.post(slackLink, { json: { text: textForSlack} },
              function (error, resp, body) {
                if (!error && resp.statusCode == 200) {
                    agent.add('Oke, ' + textForSlack +  'komt eraan!');
                }
              });
          agent.add('Oke, komt eraan!');
      }
  }

  function stringToNumber(numbersInString){
      var ref = { een:1, twee:2, drie:3, vier:4, vijf:5, zes:6, zeven:7, acht:8, negen:9, tien:10, elf:11, twaalf:12, dertien:13, veertien:14, vijftien:15, zestien:16, zeventien:17, achttien:18, negentien:19, twintig:20, dertig: 30, veertig: 40, vijftig: 50, zestig: 60, zeventig: 70, tachtig: 80, negentig:90 },
          mult = { honderd: 100, duizend: 1000, miljoen: 1000000 },
          strNums = numbersInString.split(' ').reverse(),
          number = 0,
          multiplier = 1;

      for(var i in strNums){
          if( mult[strNums[i]] != undefined ) {
              if(mult[strNums[i]]==100) {
                  multiplier*=mult[strNums[i]]
              }else{
                  multiplier=mult[strNums[i]]
              }
          } else {
              if (!isNaN(parseFloat(strNums[i]))) {
                  number += parseFloat(strNums[i]) * multiplier;
              } else {
                  var nums = strNums[i].split('-');
                  number += ((ref[nums[0]]||0) + (ref[nums[1]]||0)) * multiplier;
              }
          }
      }
      return number;
  }

  function containsNumber(string) {
      return /\d/.test(string);
  }

  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('test-intent', testIntent);
  intentMap.set('reservation-new', newReservation);
  intentMap.set('client-arrived', clientArrived);
  intentMap.set('client-arrived - custom', offerDrink);

  agent.handleRequest(intentMap);
});